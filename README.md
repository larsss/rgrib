# README #

R functions for parsing and manipulating Copernicus GRIB files.


```
#!r
library(rgrib)
help(read_grib)

# Read a GRIB file
grib <- read_grib( system.file("examples/interim.nc", package="rgrib") )
names(grib)

# Subset GRIB object
grib <- subset(grib, time=1)

# Fetch coordinates
crds <- boundingbox.grib(grib, var="t2m")
crds

# Melt matrix to data frame
df <- melt.grib(grib, var="t2m")
crds <- boundingbox.grib(grib, var="t2m")
map <- get_stamenmap(crds, zoom = 5, maptype = "toner-lite")

# Draw map
ggmap(map)

# Add contour lines
g <- ggmap(map,
          base_layer = ggplot(data=df, aes(x=longitude, y=latitude, z=values)))
g + geom_contour()

# Raster
g <- ggmap(map,
         base_layer = ggplot(data=df, aes(x=longitude, y=latitude, fill=values)))
g +  geom_raster(interpolate = TRUE, alpha=0.5) +
 scale_fill_gradientn(colours = rev(rainbow(7)), na.value = NA) +
 theme_bw() + coord_fixed(1.3)


# Leaflet
library(sp)
library(raster)
library(leaflet)

crdref <- CRS('+proj=longlat +datum=WGS84')
pts <- SpatialPoints(df[,1:2], proj4string=crdref)
showDefault(pts)
pts

ptsdf <- SpatialPixelsDataFrame(pts, data=df[, c("values", "ID")], tolerance = 7.62951e-05)
showDefault(ptsdf)

r <- raster(ptsdf, values=TRUE)

pal <- colorNumeric(c("red", "yellow", "transparent"), values(r),
                   na.color = "transparent")


leaflet() %>% addTiles() %>%
 addRasterImage(r, colors = pal, opacity = 0.5) %>%
 addLegend(pal = pal, values = values(r),
           title = "Temperature")

```

